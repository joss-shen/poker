
var LoginLayer = cc.Layer.extend({
    ctor:function () {

        this._super();

        var size = cc.director.getWinSize();

        var baseDir = "#img/startup/";

        var bgSprite = new cc.Sprite(baseDir+"bg.jpg");
        bgSprite.x = size.width / 2;
        bgSprite.y = size.height / 2;
        this.addChild(bgSprite);

        var girlSprite = new cc.Sprite(baseDir+"girl.png");

        girlSprite.x = size.width / 2;
        girlSprite.y = size.height / 2 - 220;

        this.addChild(girlSprite);

        var logoSprite = new cc.Sprite(res.logo);

        logoSprite.x = size.width / 2;
        logoSprite.y = size.height / 2 + 500;
        this.addChild(logoSprite);

        //var bgProgress = new cc.Sprite(baseDir+"bg-progress.png");
        //var progress = new cc.Sprite(baseDir+"progress.png");


        var poker_front = new cc.Sprite(baseDir+"poker_front.png");

        poker_front.x = size.width / 2 - 50;
        poker_front.y = size.height / 2 - 350;

        var action1 = new cc.MoveBy(1,cc.p(50,0));//0.5 seconds
        var action2 = action1.clone().reverse();

        var spriteAction = new cc.RepeatForever(new cc.Sequence(action1,action2));
        poker_front.runAction(spriteAction);

        this.addChild(poker_front);

        var poker_back = new cc.Sprite(baseDir+"poker_back.png");
        poker_back.x = size.width / 2 + 50;
        poker_back.y = size.height / 2 - 350;

        var action3 = new cc.MoveBy(1,cc.p(-50,0));//0.5 seconds
        var action4 = action3.clone().reverse();

        var spriteAction1 = new cc.RepeatForever(new cc.Sequence(action3,action4));
        poker_back.runAction(spriteAction1);
        this.addChild(poker_back);

        return true;
    }
});

function queryEntry(deviceId, callback) {
    var route = 'gate.gateHandler.queryEntry';
    pomelo.init({
        host: GC.gate_host,
        port: GC.gate_port,
        log: true
    }, function() {
        pomelo.request(route, {
            deviceId : deviceId
        }, function(data) {
            pomelo.disconnect();
            if(data.code === 500) {
                //todo 错误提示
                return;
            }
            callback(data.host, data.port);
        });
    });
};

var LoginScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new LoginLayer();
        this.addChild(layer);

        console.log('LoginScene-=-=>>');
        /*
        var deviceId = '111112';
        var url = "http://" + GC.web_host + (!!GC.web_port ? ':' + GC.web_port : '') + GC.login + '?deviceId=' + deviceId;
        getRequest(url, function (msg) {
            if(msg && msg.status == 200){
                queryEntry(deviceId,function(host,port){
                    pomelo.init({
                        host: host,
                        port: port,
                        log: true
                    }, function() {
                        var route = "connector.loginHandler.login";
                        pomelo.request(route, {
                            deviceId : deviceId
                        }, function(data) {
                            if(data.status != 200) {
                                //todo
                                return;
                            }
                            //init user
                            var user = getUser();
                            user.init(data.userInfo);

                            //change scene
                            cc.director.runScene(new LobbyScene());
                        });
                    });
                });
            }
        });
        */

        //setTimeout(function () {
        //    var lobbyScene = new LobbyScene();
        //    cc.director.runScene(new cc.TransitionFade(3.0,lobbyScene));
        //},3000);
    }
});

