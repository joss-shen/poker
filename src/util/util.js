/**
 * Created by lixiaodong on 16/4/5.
 */
var trace = function (msg) {
    cc.log(msg);
}

var getCardNameByNumber = function (number) {
    var index = '1';
    if(number > 13){
        index = Math.ceil(number / 13);
        if(number % 13 == 0){
            number -= (parseInt(number / 13) - 1)* 13;
        } else {
            number -= parseInt(number / 13) * 13;
        }
    }
    var name = 'card-' + index+'0';

    switch (number){
        case 0:
            name += 'e';
            break;
        case 1:
            name += '2';
            break;
        case 2:
            name += '3';
            break;
        case 3:
            name += '4';
            break;
        case 4:
            name += '5';
            break;
        case 5:
            name += '6';
            break;
        case 6:
            name += '7';
            break;
        case 7:
            name += '8';
            break;
        case 8:
            name += '9';
            break;
        case 9:
            name += 'a';
            break;
        case 10:
            name += 'b';
            break;
        case 11:
            name += 'c';
            break;
        case 12:
            name += 'd';
            break;
        case 13:
            name += 'e';
            break;
    }
    return name+'.png';
}